# ReadMe ProjektRoger #

#### Vad ProjektRoger är ####
ProjektRoger är en konsolapplikation som skall användas för att hålla en förteckning över Svenska Kyrkans webbplatser med tillhörande applikationer samt vilka olika beroenden som finns däremellan. 

Syftet är att medarbetarna (läs utvecklarna) skall enkelt kunna ha en bra översikt över vilka versioner som ligger skarpt i miljöerna och att informationen hämtas direkt från källan när den behövs.

#### Vad ProjektRoger inte är ####
ProjektRoger är inte ett verktyg som används för att skriva över information i filerna den söker genom. Det är inte heller ett visuellt program så informationen som lagras kan inte presenteras för användaren (for now...)

----------

### Requirements ###
- JDK 1.8
- SQLite
- Log4j2

----------

### Inledande argument ###

Programmet kan köras på två olika sätt.

1. `--server-set`

Det andra argumentet skall vara `domain\server name`, det tredje skall vara IP-adress. När programmet körs sätts denna IP-adress sedan som IP-adress till den aktuella servern i databasen.

2. `domain\server name + filepaths`

Applikationen söker genom de valda sökvägarna efter `.config`-filer, letar efter `applications`, `bindings`, `applicationPools`, `websites`, `appSettings` samt `linkedApplications`. Därefter mappas dessa objekt + servern som kör programmet och  allt sparas i en SQLite-Db.


----------

###Exempel på användningsområde ###

	SELECT * FROM linkedApplications LEFT JOIN application
			ON application_applicationID = applicationID
			LEFT JOIN appSetting ON appSetting_appSettingID = appSettingID
			LEFT JOIN website ON website_websiteID = websiteID
			LEFT JOIN applicationPool ON applicationPool_applicationPoolID = applicationPoolID
			LEFT JOIN server ON server_serverID = serverID;

Av att köra till exempel den här SQL-queryn får man ut information som;

- Vilken server som "äger" hemsidan.
- Vilken hemsida som nyttjar applikationen.
- Vilken/vilka appSettings applikationen har.
- Vart (vilken file path) appSettingen ligger.
- Vilken server som äger applicationPoolen.
- Information om alla olika delar (vilket IP, vilket namn, när någon post senast blev uppdaterad etc.)

*Queryn är körd via [SQlite Browser](http://sqlitebrowser.org/) där databasfilen är intankad.*

----------

### Tekniker som använts ###

- Java (iterationer, metoder, hashmaps, arraylists, objekt etc.)
- Log4j2
- Singleton Pattern (kind of)
- SAX XML parser
- Recursion
- Error handling (try/catch)
- SQLite
- Prepared Statements
- Regex
- I/O