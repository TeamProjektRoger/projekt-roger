package com.dataductus.ProjektRoger;

import java.sql.SQLException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.ThreadContext;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Db.inserts.InsertAppSetting;
import com.dataductus.ProjektRoger.Db.inserts.InsertApplicationPool;
import com.dataductus.ProjektRoger.Db.inserts.InsertServer;
import com.dataductus.ProjektRoger.Holders.AppSetting;
import com.dataductus.ProjektRoger.Holders.ApplicationPool;
import com.dataductus.ProjektRoger.Holders.LinkedApplication;
import com.dataductus.ProjektRoger.Holders.Server;
import com.dataductus.ProjektRoger.XMLandFiles.FindFiles;

public class App {

	private static final Logger log = LogManager.getLogger(App.class.getName());

	public static void main(String[] args) throws SQLException {
		
		String userName = System.getProperties().getProperty("user.name");
		
		ThreadContext.put("user", userName);
		log.debug("--Session started--");
		DbManager database = new DbManager("Db/RobertsProjekt.db");
		database.CreateDb("SQL/createDb.txt");
		Server server = new Server();
		InsertServer insertServer = new InsertServer(server);
		
		if (args[0].equalsIgnoreCase("--server-set")) {
		
			String [] parts = splitDomainAndName(args[1]);
			server.setDomain(parts[0]);
			server.setName(parts[1]);
			server.setIP(args[2]);
			database.runServerCommand(insertServer);
			log.info(args[1] + "'s IP has been set to " + args[2]);
		}
		else if (!args[0].contains("\\") && !args[1].contains("\\")) {
			log.info("User did not enter initial arguments correctly.");
		}
		else {
			String [] parts = splitDomainAndName(args[0]);
			server.setDomain(parts[0]);
			server.setName(parts[1]);
			database.runServerCommand(insertServer);
			
			for (int i = 1; i < args.length; i++) {
				log.info(args[i] + " is about to be searched.");
				FindFiles.DirectorySearch(server, args[i]);
			}

			for (ApplicationPool applicationPool : server.getAppPool().values()) {
				InsertApplicationPool insertAppPool = new InsertApplicationPool(applicationPool);
				database.runCommand(insertAppPool);
				DbManager.dbPostCounter++;
			}
			for (AppSetting appSetting : server.getAppSettingList()) {
				InsertAppSetting insertAppSetting = new InsertAppSetting(appSetting);
				database.runCommand(insertAppSetting);
				DbManager.dbPostCounter++;
			}
			database.insertToDb(server.getWebsites());
			
			LinkedApplication linkedApp = new LinkedApplication();
			linkedApp.buildLinkedApplication(database);
		}
		log.debug("--Session ended--");
	}
	
	public static String [] splitDomainAndName (String splitThis) {

		String [] parts = splitThis.split("\\\\");
		return parts;
	}
}