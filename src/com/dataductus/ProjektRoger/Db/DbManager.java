package com.dataductus.ProjektRoger.Db;
import java.io.File;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Scanner;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dataductus.ProjektRoger.Db.inserts.InsertApplication;
import com.dataductus.ProjektRoger.Db.inserts.InsertBinding;
import com.dataductus.ProjektRoger.Db.inserts.InsertInterface;
import com.dataductus.ProjektRoger.Db.inserts.InsertServer;
import com.dataductus.ProjektRoger.Db.inserts.InsertWebsite;
import com.dataductus.ProjektRoger.Holders.*;


public class DbManager {

	private static final Logger log = LogManager.getLogger(DbManager.class);

	private String SQLfile;
	ResultSet rs;
	PreparedStatement preparedStmt = null;
	Statement stmt = null;
	Connection c = null;
	public static int dbPostCounter = 1;

	public DbManager (String SQLPathfile) {

		SQLfile = SQLPathfile;
	}

	public Connection initConnection() {	//initialize db-connection

		if (c != null) {		//singleton-connection.
			return c;
		}
		try {
			Class.forName("org.sqlite.JDBC");
			c = DriverManager.getConnection("jdbc:sqlite:" + SQLfile);
		}
		catch (Exception ex){
			log.error(ex.getStackTrace());
		}
		return c;
	}

	public void Execute(String query, ArrayList <Object> values) throws SQLException {	//use for inserts, updates, deletes

		try {
			if (values != null) {

				preparedStmt = initConnection().prepareStatement(query, Statement.RETURN_GENERATED_KEYS);

				for (int i = 0; i < values.size(); i++) {
					preparedStmt.setObject(i + 1, values.get(i));
				}
				preparedStmt.executeUpdate();
			}
			else {
				stmt = initConnection().createStatement();
				stmt.executeUpdate(query);
			}

		} catch (SQLException ex) {
			log.error(ex.getClass().getName() + " " + ex.getMessage());
		}
	}

	public ResultSet executeQuery(String query) throws SQLException {	//use for selects
		Statement stmt = initConnection().createStatement();
		ResultSet r = stmt.executeQuery(query);
		return r;
	}

	public void CreateDb (String dbFile) {
		try {
			@SuppressWarnings("resource")
			String text = new Scanner(new File(dbFile), "UTF-8").useDelimiter("\\A").next();	//reads createDb.txt
			Execute(text, null);
		} catch (Exception ex) {
			log.error(ex.getClass().getName() + " " + ex.getMessage());
		}
	}
	
	public void runCommand(InsertInterface runThis) throws SQLException {
		runThis.insertToDb(this);
	}
	
	public void runServerCommand (InsertServer server) throws SQLException {
		server.insertToDb(this);
	}
	
	public void insertToDb (ArrayList<Website> siteList) throws SQLException {

		for (int i = 0; i < siteList.size(); i++) { //inserts websites.
			InsertWebsite website = new InsertWebsite(siteList.get(i));
			runCommand(website);
			dbPostCounter++;
		}

		for (int j = 0; j < siteList.size(); j++) { //inserts applications
			for (Application application : siteList.get(j).getAppList()) {
				InsertApplication app = new InsertApplication(application);
				runCommand(app);
				dbPostCounter++;
			}
		}

		for (int k = 0; k < siteList.size(); k++) { //inserts bindings.
			for (Binding binding : siteList.get(k).getBindingList()) {
				InsertBinding insertBinding = new InsertBinding(binding);
				runCommand(insertBinding);
				dbPostCounter++;
			}
		}
		log.info(dbPostCounter + " posts were successfully added/updated to Db.");
	}
	
	public String getTime () {		

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Calendar cal = Calendar.getInstance();
		String currentTime = dateFormat.format(cal.getTime());
		return currentTime;
	}
}