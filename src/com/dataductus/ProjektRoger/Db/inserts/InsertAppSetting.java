package com.dataductus.ProjektRoger.Db.inserts;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Holders.AppSetting;

public class InsertAppSetting implements InsertInterface {

	private AppSetting appSetting;
	ResultSet rs;

	public InsertAppSetting(AppSetting appSetting) {
		this.appSetting = appSetting;
	}

	public void insertToDb (DbManager manager) throws SQLException {

		String selectAppSetting = "SELECT * FROM appSetting WHERE URL = '" + appSetting.getUrl() + "';";
		String updateAppSetting = "UPDATE appSetting SET name = ?, URL = ?, foundInPath = ?, post_updated = ? WHERE URL = ?;";
		String insertAppSetting = "INSERT INTO appSetting (appSettingID, name, URL, foundInPath) VALUES (?, ?, ?, ?);";

		ArrayList<Object> values = new ArrayList<Object>();	

		rs = manager.executeQuery(selectAppSetting);

		if (rs.next()) {
			do {
				values.add(appSetting.getName());
				values.add(appSetting.getUrl());
				values.add(appSetting.getFoundInPath());
				values.add(manager.getTime());
				values.add(appSetting.getUrl());
				
				manager.Execute(updateAppSetting, values);
			} while (rs.next());
		}
		else {
			values.add(appSetting.getId());
			values.add(appSetting.getName());
			values.add(appSetting.getUrl());
			values.add(appSetting.getFoundInPath());
			
			manager.Execute(insertAppSetting, values);
		}
	}
}