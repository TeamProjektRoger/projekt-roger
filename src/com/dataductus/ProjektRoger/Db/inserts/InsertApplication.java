package com.dataductus.ProjektRoger.Db.inserts;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Holders.Application;

public class InsertApplication implements InsertInterface {

	private Application application;
	ResultSet rs;

	public InsertApplication(Application application) {
		this.application = application;
	}

	public void insertToDb (DbManager manager) throws SQLException {

		String selectApplication = "SELECT * FROM application WHERE physicalPath = '" + application.getPhysicalPath()
				+ "' AND virtualPath = '" + application.getVirtualPath() + "';";
		
		String updateApplication = "UPDATE application SET virtualPath = ?, physicalPath = ?,"
				+ "rewritePath = ?, post_updated = ? WHERE virtualPath = ? AND physicalPath = ?;";
		
		String insertApplication = "INSERT INTO application (virtualPath, physicalPath, rewritePath,"
				+ " website_websiteID, applicationPool_applicationPoolID) VALUES (?, ?, ?, ?, ?);";

		ArrayList<Object> values = new ArrayList<Object>();

		rs = manager.executeQuery(selectApplication);

		if (rs.next()) {
			values.add(application.getVirtualPath());
			values.add(application.getPhysicalPath());
			values.add(application.getRewritePath());
			values.add(manager.getTime());
			values.add(application.getVirtualPath());
			values.add(application.getPhysicalPath());
			manager.Execute(updateApplication, values);
		}
		else {
			values.add(application.getVirtualPath());
			values.add(application.getPhysicalPath());
			values.add(application.getRewritePath());
			values.add(application.website.getId());

			if (application.appPool != null) {
				values.add(application.appPool.getId());
			}
			else {
				values.add(null);
			}
			manager.Execute(insertApplication, values);
		}
	}
}