package com.dataductus.ProjektRoger.Db.inserts;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Holders.ApplicationPool;

public class InsertApplicationPool implements InsertInterface {

	private ApplicationPool appPool;
	ResultSet rs;
	
	public InsertApplicationPool(ApplicationPool applicationPool) {
		this.appPool = applicationPool;
	}
	public void insertToDb (DbManager manager) throws SQLException {

		String selectAppPool = "SELECT * FROM applicationPool WHERE name = '" + appPool.getName() + "';";
		
		String updateAppPool = "UPDATE applicationPool SET name = ?, managedRuntimeVersion = ?,"
				+ " managedPipelineMode = ?, post_updated = ? WHERE name = ?;";
		
		String insertAppPool = "INSERT INTO applicationPool (applicationPoolID, name, managedRuntimeVersion,"
				+ " managedPipelineMode, server_serverID) VALUES (?, ?, ?, ?, ?);";

		ArrayList<Object> values = new ArrayList<Object>();

		rs = manager.executeQuery(selectAppPool);

		if (rs.next()) {
			do {
				values.add(appPool.getName());
				values.add(appPool.getManagedRuntimeVersion());
				values.add(appPool.getManagedPipelineMode());
				values.add(manager.getTime());
				values.add(appPool.getName());
				manager.Execute(updateAppPool, values);
			} while (rs.next());
		} 
		else {
			values.add(appPool.getId());
			values.add(appPool.getName());
			values.add(appPool.getManagedRuntimeVersion());
			values.add(appPool.getManagedPipelineMode());
			values.add(appPool.server.getId());
			manager.Execute(insertAppPool, values);
		}
	}

}
