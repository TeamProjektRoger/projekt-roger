package com.dataductus.ProjektRoger.Db.inserts;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Holders.Binding;

public class InsertBinding implements InsertInterface {
	
	private Binding binding;
	ResultSet rs;
	
	public InsertBinding(Binding binding) {
		this.binding = binding;
	}
	
	public void insertToDb (DbManager manager) throws SQLException {

		String selectBinding = "SELECT * FROM binding where protocol = '" + binding.getProtocol()
				+ "' AND address = '" + binding.getBindingInformation() + "';";
		
		String updateBinding = "UPDATE binding SET protocol = ?, address = ?, post_updated = ? WHERE protocol = ? AND address = ?;";
		String insertBinding = "INSERT INTO binding (protocol, address, website_websiteID) VALUES (?, ?, ?)";

		ArrayList<Object> values = new ArrayList<Object>();

		rs = manager.executeQuery(selectBinding);

		if (rs.next()) {
			values.add(binding.getProtocol());
			values.add(binding.getBindingInformation());
			values.add(manager.getTime());
			values.add(binding.getProtocol());
			values.add(binding.getBindingInformation());
			manager.Execute(updateBinding, values);
		}
		else {
			values.add(binding.getProtocol());
			values.add(binding.getBindingInformation());
			values.add(binding.website.getId());
			manager.Execute(insertBinding, values);
		}
	}
	
}