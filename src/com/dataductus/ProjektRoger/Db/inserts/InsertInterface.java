package com.dataductus.ProjektRoger.Db.inserts;

import java.sql.SQLException;

import com.dataductus.ProjektRoger.Db.DbManager;

public interface InsertInterface {

	void insertToDb(DbManager manager) throws SQLException;
}