package com.dataductus.ProjektRoger.Db.inserts;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Holders.LinkedApplication;

public class InsertLinkedApplications implements InsertInterface {

	ResultSet rs;
	private LinkedApplication linkedApp;
	
	public InsertLinkedApplications(LinkedApplication linkedApp) {
		
		this.linkedApp = linkedApp;
	}
	
	public void insertToDb (DbManager manager) throws SQLException {

		String selectLinked = "SELECT * FROM linkedApplications WHERE application_applicationID = '" + linkedApp.getApplicationID()
							+ "' AND appSetting_appSettingID = '" + linkedApp.getAppSettingID() + "';";
		
		String insertLinked = "INSERT INTO linkedApplications (application_applicationID, appSetting_appSettingID) VALUES (?, ?);";
		
		String updateLinked = "UPDATE linkedApplications SET application_applicationID = ?, appSetting_appSettingID = ?,"
							+ " post_updated = ? WHERE application_applicationID = ? AND appSetting_appSettingID = ?";

		ArrayList<Object> values = new ArrayList<Object>();	

		rs = manager.executeQuery(selectLinked);

		if (rs.next()) {
			do {
				values.add(linkedApp.getApplicationID());
				values.add(linkedApp.getAppSettingID());
				values.add(manager.getTime());
				values.add(linkedApp.getApplicationID());
				values.add(linkedApp.getAppSettingID());
				manager.Execute(updateLinked, values);
				DbManager.dbPostCounter++;
			} while (rs.next());
		}
		else {
			values.add(linkedApp.getApplicationID());
			values.add(linkedApp.getAppSettingID());
			manager.Execute(insertLinked, values);
			DbManager.dbPostCounter++;
		}
	}
}
