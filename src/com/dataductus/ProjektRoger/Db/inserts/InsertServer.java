package com.dataductus.ProjektRoger.Db.inserts;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Holders.Server;

public class InsertServer {
	
	ResultSet rs;
	private Server server;

	public InsertServer(Server server) {
		this.server = server;
	}

	public Server insertToDb (DbManager manager) throws SQLException {

		String selectServer = "SELECT * FROM server WHERE name = '" + server.getName() + "';";
		String updateServer = "UPDATE server SET IP = ?, post_updated = ? WHERE name = ?;";
		String insertServer = "INSERT INTO server (serverID, name, domain, IP) VALUES(?, ?, ?, ?)";

		ArrayList<Object> values = new ArrayList<Object>();

		rs = manager.executeQuery(selectServer);

		if (rs.next()) {
			do {
				values.add(server.getIP());
				values.add(manager.getTime());
				values.add(server.getName());
				manager.Execute(updateServer, values);
				DbManager.dbPostCounter++;
			} while (rs.next());
		}
		else {
			values.add(server.getId());
			values.add(server.getName());
			values.add(server.getDomain());
			values.add(server.getIP());
			manager.Execute(insertServer, values);
			DbManager.dbPostCounter++;
		}
		return server;
	}

}