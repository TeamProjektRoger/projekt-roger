package com.dataductus.ProjektRoger.Db.inserts;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Holders.Website;

public class InsertWebsite implements InsertInterface {
	
	private Website website;
	ResultSet rs;

	public InsertWebsite(Website website) {
		this.website = website;
	}
	
	public void insertToDb(DbManager manager) throws SQLException {

		String selectWebsite = "SELECT * FROM website WHERE name = '" + website.getName() + "';";
		String updateWebsite = "UPDATE website SET name = ?, siteID = ?, post_updated = ? WHERE name = ?;";
		String insertWebsite = "INSERT INTO website (websiteID, name, siteID) VALUES (?, ?, ?);";

		ArrayList<Object> values = new ArrayList<Object>();

		rs = manager.executeQuery(selectWebsite);

		if (rs.next()) {
			do {
				values.add(website.getName());
				values.add(website.getSiteID());
				values.add(manager.getTime());
				values.add(website.getName());
				manager.Execute(updateWebsite, values);
			} while (rs.next());
		}
		else {
			values.add(website.getId());
			values.add(website.getName());
			values.add(website.getSiteID());
			manager.Execute(insertWebsite, values);
		}
	}
}