package com.dataductus.ProjektRoger.Holders;

import java.util.UUID;

public class AppSetting {
	
	private String id;
	private String name;
	private String url;
	private String foundInPath;
	
	public AppSetting () {
		 this.id = UUIDgenerator();
	}
	public String getFoundInPath() {
		return foundInPath;
	}
	public void setFoundInPath(String foundInPath) {
		this.foundInPath = foundInPath;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	
	public String UUIDgenerator() {
		
		UUID uid = UUID.randomUUID();
		return uid.toString();
	}
}