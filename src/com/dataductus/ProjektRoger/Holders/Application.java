package com.dataductus.ProjektRoger.Holders;

public class Application {
	
	private String virtualPath;		
	private String physicalPath;
	private String rewritePath;
	public ApplicationPool appPool;
	public Website website;

	public Website getWebsite() {
		return website;
	}
	public void setWebsite(Website website) {
		this.website = website;
	}
	public ApplicationPool getAppPool() {
		return appPool;
	}
	public void setAppPool(ApplicationPool appPool) {
		this.appPool = appPool;
	}
	public String getVirtualPath() {
		return virtualPath;
	}
	public void setVirtualPath(String virtualPath) {
		this.virtualPath = virtualPath;
	}
	public String getPhysicalPath() {
		return physicalPath;
	}
	public void setPhysicalPath(String physicalPath) {
		this.physicalPath = physicalPath;
	}
	public String getRewritePath() {
		return rewritePath;
	}
	public void setRewritePath(String rewritePath) {
		this.rewritePath = rewritePath;
	}

}