package com.dataductus.ProjektRoger.Holders;

import java.util.UUID;

public class ApplicationPool {
	
	private String id;
	private String name;
	private String managedRuntimeVersion;
	private String managedPipelineMode;
	public Server server;
	
	public ApplicationPool () {
		 this.id = UUIDgenerator();
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public Server getServer() {
		return server;
	}
	public void setServer(Server server) {
		this.server = server;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getManagedRuntimeVersion() {
		return managedRuntimeVersion;
	}
	public void setManagedRuntimeVersion(String managedRuntimeVersion) {
		this.managedRuntimeVersion = managedRuntimeVersion;
	}
	public String getManagedPipelineMode() {
		return managedPipelineMode;
	}
	public void setManagedPipelineMode(String managedPipelineMode) {
		this.managedPipelineMode = managedPipelineMode;
	}
	
	public String UUIDgenerator() {
		
		UUID uid = UUID.randomUUID();
		return uid.toString();
	}
}