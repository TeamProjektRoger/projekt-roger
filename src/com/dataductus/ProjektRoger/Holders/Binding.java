package com.dataductus.ProjektRoger.Holders;


public class Binding {
	
	private String protocol;
	private String bindingInformation;
	public Website website;
	
	public Website getWebsite() {
		return website;
	}
	public void setWebsite(Website website) {
		this.website = website;
	}
	public String getProtocol() {
		return protocol;
	}
	public void setProtocol(String protocol) {
		this.protocol = protocol;
	}
	public String getBindingInformation() {
		return bindingInformation;
	}
	public void setBindingInformation(String bindingInformation) {
		String trimmedBinding = bindingInformation.replaceAll("(\\*:?[0-9]+\\:?)", "");
		this.bindingInformation = trimmedBinding;
	}
}