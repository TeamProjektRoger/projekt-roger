package com.dataductus.ProjektRoger.Holders;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map.Entry;

import com.dataductus.ProjektRoger.Db.DbManager;
import com.dataductus.ProjektRoger.Db.inserts.InsertLinkedApplications;

public class LinkedApplication {

	String applicationID;
	String appSettingID;
	ResultSet rs;
	
	public String getApplicationID() {
		return applicationID;
	}
	public void setApplicationID(String applicationID) {
		this.applicationID = applicationID;
	}
	public String getAppSettingID() {
		return appSettingID;
	}
	public void setAppSettingID(String appSettingID) {
		this.appSettingID = appSettingID;
	}

	/**
	 * Creates all possible URL:s from [binding.protocol + :// + binding.bindingInformation + application.virtualPath]
	 * Then compares these with appSettings URL, if it finds a match -> a linked application is created.
	 */
	public void buildLinkedApplication (DbManager manager) throws SQLException {

		HashMap<String, ArrayList<String>> appURLlist = new HashMap<String, ArrayList<String>>();
		HashMap<String, String> appSettingList = new HashMap<>();
		rs = manager.executeQuery("SELECT * FROM binding LEFT JOIN application ON application.website_websiteID = binding.website_websiteID;");
		
		while (rs.next()) {
			String currentURL = rs.getString("protocol") + "://" + rs.getString("address") + rs.getString("virtualPath");
			String appID = rs.getString("applicationID");
			
			if (appURLlist.get(appID) == null) {
				ArrayList<String> url = new ArrayList<>();
				url.add(currentURL);
				appURLlist.put(appID, url);
			}
			else {
				appURLlist.get(appID).add(currentURL);
			}
		}
		
		rs = manager.executeQuery("SELECT * FROM appSetting");
		
		while(rs.next()) {
			String appSettingID = rs.getString("appSettingID");
			String url = rs.getString("URL");
			appSettingList.put(appSettingID, url);
		}

		for(Entry<String, ArrayList<String>> e1: appURLlist.entrySet()) { //applications
			for(Entry<String, String> e2: appSettingList.entrySet()) {	//appSettings
				if (e1.getValue().contains(e2.getValue())) {
					
					LinkedApplication linkedApp = new LinkedApplication();
					linkedApp.setApplicationID(e1.getKey());
					linkedApp.setAppSettingID(e2.getKey());
					
					InsertLinkedApplications insertLinked = new InsertLinkedApplications(linkedApp);
					manager.runCommand(insertLinked);
				}
			}
		}
	}
	
}