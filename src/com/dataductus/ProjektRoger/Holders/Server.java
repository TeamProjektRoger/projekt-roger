package com.dataductus.ProjektRoger.Holders;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.UUID;

public class Server {
	
	private String id;
	private String name;
	private String IP;
	private String domain;
	private ArrayList<Website> websites = new ArrayList<>();
	private HashMap<String, ApplicationPool> appPool = new HashMap<>();
	private ArrayList <AppSetting> appSettingList = new ArrayList<>();
	
	public Server() {
		this.id = UUIDgenerator();
	}
	
	public AppSetting createAppSetting () {
		
		AppSetting appSetting = new AppSetting();
		appSettingList.add(appSetting);
		return appSetting;
	}

	public Website createWebsite () {
		
		Website website = new Website();
		websites.add(website);
		return website;
	}

	public ApplicationPool getOrCreateApplicationPool (String applicationPoolName) {

		if (!appPool.containsKey(applicationPoolName)) { 
			ApplicationPool applicationPool = new ApplicationPool();
			applicationPool.setName(applicationPoolName);
			applicationPool.setServer(this);
			appPool.put(applicationPoolName, applicationPool);
		}
		return appPool.get(applicationPoolName);
	}


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getDomain() {
		return domain;
	}
	public void setDomain(String domain) {
		this.domain = domain;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getIP() {
		return IP;
	}
	public void setIP(String iP) {
		IP = iP;
	}
	public ArrayList<Website> getWebsites() {
		return websites;
	}

	public void setWebsites(ArrayList<Website> websites) {
		this.websites = websites;
	}

	public HashMap<String, ApplicationPool> getAppPool() {
		return appPool;
	}

	public void setAppPool(HashMap<String, ApplicationPool> appPool) {
		this.appPool = appPool;
	}
	
	public ArrayList<AppSetting> getAppSettingList() {
		return appSettingList;
	}

	public void setAppSettingList(ArrayList<AppSetting> appSettingList) {
		this.appSettingList = appSettingList;
	}


	public String UUIDgenerator() {
		
		UUID uid = UUID.randomUUID();
		return uid.toString();
	}
	
}