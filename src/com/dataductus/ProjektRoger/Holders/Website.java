package com.dataductus.ProjektRoger.Holders;

import java.util.ArrayList;
import java.util.UUID;

public class Website {
	
	private String id;
	private String name;
	private String siteID;
	private ArrayList <Application> appList = new ArrayList<>();
	private ArrayList <Binding> bindingList = new ArrayList<>();
	
	public Website () {
		 this.id = UUIDgenerator();
	}
	public Application createApplication () {
		
		Application application = new Application();
		application.setWebsite(this);
		appList.add(application);
		return application;
	}
	
	public Binding createBinding () {
		
		Binding binding = new Binding();
		binding.setWebsite(this);
		bindingList.add(binding);
		return binding;
	}
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSiteID() {
		return siteID;
	}
	public void setSiteID(String siteID) {
		this.siteID = siteID;
	}
	public ArrayList<Application> getAppList() {
		return appList;
	}
	public void setAppList(ArrayList<Application> appList) {
		this.appList = appList;
	}
	public ArrayList<Binding> getBindingList() {
		return bindingList;
	}
	public void setBindingList(ArrayList<Binding> bindingList) {
		this.bindingList = bindingList;
	}
	
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

	public String UUIDgenerator() {
		
		UUID uid = UUID.randomUUID();
		return uid.toString();
	}
}