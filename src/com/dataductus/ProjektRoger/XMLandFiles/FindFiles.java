package com.dataductus.ProjektRoger.XMLandFiles;
import java.io.File;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.dataductus.ProjektRoger.Holders.Server;

public class FindFiles {

	private static final Logger log = LogManager.getLogger(FindFiles.class.getName());
	
	public static void DirectorySearch(Server server, String dirSearch) {
		
		File dir = new File(dirSearch);
		try {
			for (File f : dir.listFiles()) {

				if (f.getName().endsWith(".config")) {
					SAXHandler.parseXML(server, f);
					log.info(f.getName() + " was successfully parsed.");
				}
				if (f.isDirectory()) {
					DirectorySearch(server, f.getAbsolutePath());
				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
			log.warn("Incorrect file path - " + dirSearch);
		}
	}
}