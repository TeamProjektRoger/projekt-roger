package com.dataductus.ProjektRoger.XMLandFiles;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import com.dataductus.ProjektRoger.Holders.*;


public class SAXHandler extends DefaultHandler {
	
	private static final Logger log = LogManager.getLogger(SAXHandler.class.getName());
	
	String virtualPath = "";
	private Website currentWebsite;
	private Binding currentBinding;
	private Application currentApp;
	private ApplicationPool currentPool;
	private Server server;
	private AppSetting currentAppSetting;
	static String currentFile;
	
	ArrayList<String> xpath = new ArrayList<String>();	//once parser comes across a start element, adds it here.

	//these are the paths we're looking for.
	String add			= "[configuration, appSettings, add]";
	String site			= "[configuration, system.applicationHost, sites, site]";
	String appPools		= "[configuration, system.applicationHost, applicationPools, add]";
	String app			= "[configuration, system.applicationHost, sites, site, application]";
	String binding		= "[configuration, system.applicationHost, sites, site, bindings, binding]";
	String rewriteAdd	= "[configuration, system.webServer, rewrite, rewriteMaps, rewriteMap, add]";
	String virtual		= "[configuration, system.applicationHost, sites, site, application, virtualDirectory]";

	private SAXHandler(Server serv) {
	
		server = serv;
	}
	
	public static void parseXML (Server server, File f) throws IOException, ParserConfigurationException {
		try {
			SAXParserFactory factory = SAXParserFactory.newInstance();
			SAXParser parser = factory.newSAXParser();     
			SAXHandler handler = new SAXHandler(server);    	
			parser.parse(f, handler);
			currentFile = f.toString();
		} catch (SAXException ex) {
			log.error(ex.getClass().getName() + " " + ex.getMessage());
		}
	}
//
//	public void startDocument() throws SAXException { }

	public void startElement(String uri, String localName, String qName, 
			Attributes attributes) throws SAXException {
		
		xpath.add(qName);	//adds start element to 'xpath'

		if (xpath.toString().equals(site)) {
			buildSite(attributes);
		}
		else if (xpath.toString().equals(binding)) {
			buildBinding(attributes);
		}
		else if (xpath.toString().equals(app)) {
			buildApplication(attributes, true);
		}
		else if (xpath.toString().equals(virtual)) {
			buildApplication(attributes, false);
		}
		else if (xpath.toString().equals(appPools)) {
			buildApplicationPool(attributes);
		}
		else if (xpath.toString().equals(add)) {
			buildAppSetting(attributes);
		}
	}

	public void endElement(String uri, String localName, String qName) throws SAXException {

		if (xpath.toString().equals(app)) {
			currentApp.setVirtualPath(virtualPath);
			virtualPath = "";
		}
		xpath.remove(xpath.size() - 1);	//remove the latest start element from xpath.
	}

//	public void endDocument() throws SAXException { }
//
//	public void characters(char[] buffer, int start, int length) { }
	
	public void buildSite(Attributes attributes) {
		
		int length = attributes.getLength();
		currentWebsite = server.createWebsite();
		for (int i = 0; i < length; i++) {
			String attributeName = attributes.getQName(i);
			String attributeValue = attributes.getValue(i);
			
			if (attributeName.equals("name")) {
				currentWebsite.setName(attributeValue);
			}
			else if (attributeName.equals("id")) {
				currentWebsite.setSiteID(attributeValue);
			}
		}
	}
	public void buildBinding(Attributes attributes) {
		
		int length = attributes.getLength();
		currentBinding = currentWebsite.createBinding();
		for (int i = 0; i < length; i++) {
			String attributeName = attributes.getQName(i);
			String attributeValue = attributes.getValue(i);
			
			if (attributeName.equals("protocol")) {
				currentBinding.setProtocol(attributeValue);
			}
			else if (attributeName.equals("bindingInformation")) {
				currentBinding.setBindingInformation(attributeValue);
			}
		}
	}
	
	public void buildApplication(Attributes attributes, boolean isApplication) {
		
		int length = attributes.getLength();
		
		if (isApplication) {
			currentApp = currentWebsite.createApplication();
		}

		for (int i = 0; i < length; i++) {
			
			String attributeName = attributes.getQName(i);
			String attributeValue = attributes.getValue(i);
			
			//application physicalPath and virtualPath extraction.
			if (attributeName.equals("path")) {		//second/third/fourth part of virtualPath
				virtualPath += attributeValue;
			}
			else if (attributeName.equals("physicalPath")) {
				currentApp.setPhysicalPath(attributeValue);
			}
			else if (attributeName.equals("applicationPool")) {
				currentApp.setAppPool(server.getOrCreateApplicationPool(attributeValue));
				
			}
		}
	}
	public void buildApplicationPool (Attributes attributes) {
		
		int length = attributes.getLength();
		String applicationPoolName = null;
		String managedRuntimeVersion = null;
		String managedPipelineMode = null;
		
		for (int i = 0; i < length; i++) {
			
			String attributeName = attributes.getQName(i);
			String attributeValue = attributes.getValue(i);
			
			if (attributeName.equals("name")) {
				applicationPoolName = attributeValue;
			}
			else if (attributeName.equals("managedRuntimeVersion")) {
				managedRuntimeVersion = attributeValue;
			}
			else if (attributeName.equals("managedPipelineMode")) {
				managedPipelineMode = attributeValue;
			}
		}
		if (applicationPoolName == null) return; //jumps out of method.
		currentPool = server.getOrCreateApplicationPool(applicationPoolName);
		currentPool.setManagedRuntimeVersion(managedRuntimeVersion);
		currentPool.setManagedPipelineMode(managedPipelineMode);
	}
	

	private void buildAppSetting(Attributes attributes) {
		
		int length = attributes.getLength();
		currentAppSetting = server.createAppSetting();
		for (int i = 0; i < length; i++) {
			
			String attributeName = attributes.getQName(i);
			String attributeValue = attributes.getValue(i);
			
			if (attributeName.equals("key")) {
				currentAppSetting.setName(attributeValue);
			}
			else if (attributeName.equals("value")) {
				currentAppSetting.setUrl(attributeValue);
				currentAppSetting.setFoundInPath(currentFile);
			}
		}
	}
	
}